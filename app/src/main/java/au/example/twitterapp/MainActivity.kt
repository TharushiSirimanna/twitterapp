package au.example.twitterapp
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.add_ticket.view.*
import kotlinx.android.synthetic.main.tweets_ticket.view.*
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity() {

    private var database = FirebaseDatabase.getInstance()
    private var myRef = database.reference

    var ListTweets = ArrayList<ticket>()
    var adapter:TweetsAdapter?=null
    var myemail:String?=null
    var UserUID:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var b:Bundle?=intent.extras
        myemail=b?.getString("email")
        UserUID=b?.getString("uid")
        ListTweets.add(ticket("0", "him", "url", "add"))
        adapter = TweetsAdapter(this, ListTweets)
        lvTweets.adapter=adapter
        LoadPost()
    }

    inner class TweetsAdapter : BaseAdapter {

        var listTweetsAdapter = ArrayList<ticket>()
        var context: Context? = null

        constructor(context: Context, listTweetsAdapter: ArrayList<ticket>) : super() {
            this.listTweetsAdapter = listTweetsAdapter
            this.context = context

        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

               var myTweet=listTweetsAdapter[position]

            if (myTweet.tweetPersonUID.equals("add")) {

                var myView = layoutInflater.inflate(R.layout.add_ticket, null)
                myView.iv_attach.setOnClickListener(View.OnClickListener {
                    loadimage()

                })

                myView.iv_post.setOnClickListener(View.OnClickListener {


                    myRef.child("posts").push().setValue(
                        PostInfo(UserUID!!,
                            myView.etPost.text.toString(), DownloadURL!!))
                        myView.etPost.setText("")

                })

                return myView
            } else if (myTweet.tweetPersonUID.equals("loading")){
                var myView = layoutInflater.inflate(R.layout.loading_ticket,null)
                return myView
            }else{

                var myView = layoutInflater.inflate(R.layout.tweets_ticket,null)
                myView.txt_tweet.setText(myTweet.tweetText)
                myView.txtUserName.setText(myTweet.tweetPersonUID)
//                 myView.tweet_picture.setImageURI(myTweet.tweetImageURL)
                Picasso.get().load(myTweet.tweetImageURL).into(myView.tweet_picture)
//                 Picasso.with(context).load(myTweet.tweetImageURL).into(myView.tweet_picture)


                myRef.child("Users").child(myTweet.tweetPersonUID!!)
                    .addValueEventListener(object :ValueEventListener {

                        override fun onCancelled(p0: DatabaseError) {

                        }

                        override fun onDataChange(p0: DataSnapshot) {

                            try {


                                var td = p0!!.value as HashMap<String, Any>

                                for (key in td.keys) {

                                    var userInfo = td[key] as String
                                    if(key.equals("Profile Image")){
                                        Picasso.get().load(userInfo).into(myView.picture_path)
//                                        Picasso.with(context).load(myTweet.tweetImageURL).into(myView.picture_path)

                                    }else{
                                        myView.txtUserName.setText(userInfo)
                                    }

                                }

                                adapter!!.notifyDataSetChanged()
                            } catch (ex: Exception) {
                            }
                        }


                    })


                return myView
            }
        }

        override fun getItem(position: Int): Any {
            return listTweetsAdapter[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
           return listTweetsAdapter.size
        }

    }

    val PICK_IMAGE_CODE = 123
    fun loadimage() {

        var intent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(intent, PICK_IMAGE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE_CODE && data != null && resultCode == RESULT_OK) {


            val selectedImage = data.data
            val filePathColum = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = contentResolver.query(selectedImage!!, filePathColum, null, null, null)
            cursor?.moveToFirst()
            val columIndex = cursor?.getColumnIndex(filePathColum[0])
            val picturePath = cursor?.getString(columIndex!!)
            cursor?.close()
            UploadImage(BitmapFactory.decodeFile(picturePath))

        }


    }


    var DownloadURL:String?=""
    fun UploadImage(bitmap: Bitmap){

        ListTweets.add(0,ticket("0", "him", "url", "loading"))
        adapter!!.notifyDataSetChanged()
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.getReferenceFromUrl("gs://twitterapp-c76d1.appspot.com")
        val df = SimpleDateFormat("ddMMyyHHmmss")
        val dataobj = Date()
        val imagePath = SplitString(myemail!!) + "." + df.format(dataobj) + ".jpg"
        val imageRef = storageRef.child("imagePost/" + imagePath)
        val boas = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, boas)
        val data = boas.toByteArray()
        val uploadTask = imageRef.putBytes(data)
        uploadTask.addOnFailureListener {

            Toast.makeText(applicationContext, "fall to upload", Toast.LENGTH_LONG).show()

        }.addOnSuccessListener { taskSnapshot ->

            DownloadURL = taskSnapshot.storage.downloadUrl.toString()
            ListTweets.removeAt(0)
            adapter!!.notifyDataSetChanged()

        }

    }

    fun SplitString(email: String): String {
        val split = email.split("@")
        return split[0]

    }

     fun LoadPost(){

         myRef.child("posts")
              .addValueEventListener(object :ValueEventListener {
                  override fun onCancelled(p0: DatabaseError) {


                  }

                  override fun onDataChange(p0: DataSnapshot) {
                      try {

                          ListTweets.clear()
                          ListTweets.add(ticket("0", "him", "url", "add"))

                          var td = p0!!.value as HashMap<String, Any>

                          for (key in td.keys) {

                              var post = td[key] as HashMap<String, Any>

                              ListTweets.add(
                                  ticket(
                                      key,
                                      post["text"] as String,
                                      post["postImage"] as String,
                                      post["userUID"] as String
                                  )
                              )
                          }

                          adapter!!.notifyDataSetChanged()
                      } catch (ex: Exception) {
                      }
                  }


              })


     }
}




